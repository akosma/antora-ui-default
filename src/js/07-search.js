; (function () {
  'use strict'

  const testJSON = [{ path: '/blog/starting-a-typescript-cli-project-from-scratch/', title: 'Starting a Typescript CLI Project from Scratch', snippet: 'The JavaScript ecosystem has grown dramatically in the past decade. It has become so complex, that I\u2019ve seen many new\u2026' }, { path: '/blog/making-ios-applications-accessible/', title: 'Making iOS Applications Accessible', snippet: 'This is the talk I gave at the Mobile Developer Summit, Bangalore, India, September 15th, 2016 Thanks everyone for attending\u2026' }, { path: '/blog/refactoring-ios-projects/', title: 'Refactoring iOS Projects', snippet: 'Presentation given in Dnipropetrovsk, Ukraine, on July 16th, 2016. In this session we are going to learn simple yet effective\u2026' }]

  // Checks whether a string is empty, blank, null or undefined
  function isEmptyOrBlank (str) {
    return (!str || str.length === 0 || !str.trim())
  }

  // Removes all the children of a node passed as parameter
  function removeAllChildren (node) {
    while (node.firstChild) {
      node.removeChild(node.firstChild)
    }
  }

  // Creates the DOM structure of a single search result item
  // The website variable contains the current domain where this code is running.
  function createSearchResultsDiv (item, website) {
    var searchParagraph = document.createElement('p')
    searchParagraph.className = 'search-paragraph'

    var searchEntry = document.createElement('a')
    searchEntry.innerText = item.title
    searchEntry.href = item.path
    searchEntry.className = 'search-entry'
    searchParagraph.appendChild(searchEntry)

    var br1 = document.createElement('br')
    searchParagraph.appendChild(br1)

    var searchLink = document.createElement('a')
    searchLink.innerText = website + item.path
    searchLink.href = item.path
    searchLink.className = 'search-link'
    searchParagraph.appendChild(searchLink)

    var br2 = document.createElement('br')
    searchParagraph.appendChild(br2)

    var searchExcerpt = document.createElement('span')
    searchExcerpt.className = 'search-excerpt'
    searchExcerpt.innerText = item.snippet
    searchParagraph.appendChild(searchExcerpt)

    var searchDiv = document.createElement('div')
    searchDiv.className = 'search-div paragraph'
    searchDiv.onclick = function (e) {
      if (e.target !== searchEntry) {
        // Don't trigger event if right-clicking on a search result
        window.location.href = item.path
      }
    }
    searchDiv.appendChild(searchParagraph)
    return searchDiv
  }

  // Builds the HTML structure of the list of search results
  // The results variable is an array of objects with 'name', 'href' and 'excerpt' keys.
  // The query variable is a string entered by the user.
  function display (results, query) {
    if (isEmptyOrBlank(query)) {
      // Display the original page in lieu of the search results if not done yet
      if (!mainArticle.parentNode) {
        contentDiv.replaceChild(mainArticle, searchArticle)
        if (toc) toc.style.visibility = 'visible'
      }
      return
    }
    // Rebuild the contents of the "search results" page
    removeAllChildren(searchArticle)
    var searchTitle = document.createElement('h1')
    searchTitle.className = 'page'
    searchArticle.appendChild(searchTitle)
    searchTitle.innerText = 'Search Results for "' + query.trim() + '"'
    if (results.length === 0) {
      var searchResult = document.createElement('p')
      searchResult.innerText = 'No results found.'
      searchArticle.appendChild(searchResult)
    } else {
      results.forEach(function (item, idx) {
        var searchDiv = createSearchResultsDiv(item, website)
        searchArticle.appendChild(searchDiv)
      })
    }
    // Replace the current page with a "search results" page if not done yet
    if (!searchArticle.parentNode) {
      if (toc) toc.style.visibility = 'hidden'
      contentDiv.replaceChild(searchArticle, mainArticle)
    }
  }

  // Performs the actual search
  async function search (query) {
    if (isEmptyOrBlank(query)) {
      // Display the original page in lieu of the search results if not done yet
      if (!mainArticle.parentNode) {
        window.body.replaceChild(mainArticle, searchArticle)
        document.title = mainTitle
      }
      return
    }
    if (window.location.hostname === 'localhost') {
      return testJSON
    }
    const url = '/search/index.php?q=' + encodeURIComponent(query)
    const response = await window.fetch(url)
    const json = await response.json()
    return json.results
  }

  var contentDiv = document.querySelector('.content')
  var mainArticle = document.querySelector('.doc')
  var mainTitle = ''
  var searchInput = document.querySelector('#search-input')
  var searchButton = document.querySelector('.search-button')
  var toc = document.querySelector('.sidebar')
  var website = window.location.protocol + '//' + window.location.host

  // Just to make sure that there is a place where to show search results
  if (!contentDiv || !mainArticle || !searchInput) {
    console.error('Not found required elements in page with CSS classes "main" and "doc",')
    console.error('or a text field with ID "search-input".')
    return
  }

  // Create a placeholder node to show search results
  var searchArticle = document.createElement('article')
  searchArticle.className = 'doc'

  // Timeout to hold 500 milliseconds before searching
  var timeout = null

  // Clears timeout and searches immediately
  async function searchNow () {
    const query = searchInput.value
    if (isEmptyOrBlank(query)) {
      // Display the original page in lieu of the search results if not done yet
      if (!mainArticle.parentNode) {
        window.body.replaceChild(mainArticle, searchArticle)
        document.title = mainTitle
      }
      return
    }
    if (timeout) clearTimeout(timeout)
    const results = await search(query)
    display(results, query)
    updateURL(results, query)
  }

  // Updates URL field when user searches
  function updateURL (results, query) {
    const state = {
      query: query,
      results: results,
    }
    window.history.pushState(state, 'Search | The OpenShift Guide', '/search/?q=' + encodeURIComponent(query))
  }

  // Handles the back button to go back
  // and forth across search results
  window.onpopstate = function (e) {
    if (e.state) {
      searchInput.value = e.state.query
      display(e.state.results, e.state.query)
      document.title = 'Search'
    } else {
      searchInput.value = ''
      contentDiv.replaceChild(mainArticle, searchArticle)
      document.title = mainTitle
    }
  }

  // Clears the previous timeout if any, and sets a new one
  // to search in 500 milliseconds
  function triggerDelayedSearch () {
    if (timeout) clearTimeout(timeout)
    timeout = setTimeout(function () {
      searchNow()
    }, 500)
  }

  // Event to be fired everytime the user presses a key
  searchInput.addEventListener('keyup', function () {
    triggerDelayedSearch()
  })

  // Event to be fired when the input gains focus
  searchInput.addEventListener('focus', function () {
    triggerDelayedSearch()
  })

  // Event to be fired by a search page exposed through OpenSearch
  searchInput.addEventListener('search', function () {
    searchNow()
  })

  // If the user presses enter, search directly
  searchInput.addEventListener('keydown', async function (event) {
    if (event.keyCode === 13) {
      await searchNow()
    }
  })

  // If the user clicks on the search icon, search directly
  searchButton.addEventListener('click', async function (event) {
    await searchNow()
  })
})()
